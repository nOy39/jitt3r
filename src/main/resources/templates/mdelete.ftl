<div id="modal-trash" uk-modal xmlns="http://www.w3.org/1999/html">

    <div class="uk-modal-dialog uk-modal-body">
        <form method="post" action="/desk/delete">
            <h2 class="uk-modal-title">Delete desk</h2>
            <p>Are you sure???</p>
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary" type="submit">Delete</button>
            </p>
        </form>
    </div>

</div>