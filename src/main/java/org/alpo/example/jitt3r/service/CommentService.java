package org.alpo.example.jitt3r.service;

import org.alpo.example.jitt3r.entity.Comment;
import org.alpo.example.jitt3r.entity.Note;
import org.alpo.example.jitt3r.entity.User;
import org.alpo.example.jitt3r.repos.CommentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
@Service
public class CommentService{

    @Autowired
    private CommentRepo commentRepo;


    /**
     * Метод добавляет коментарий.
     * @param comment - текст коментария
     * @param author - автор коментария
     * @param currentNote - нота в которой пишем коментарий
     * @return - возвращаем успешный ответ выполнения метода
     */
    public boolean addNewComment(String comment, User author, Note currentNote) {

        Comment newComment = new Comment(author,
                comment,
                currentNote,
                0,
                0,
                currentNote.getProject());
        commentRepo.save(newComment);

        return true;
    }

    /**
     *  Метод добаляет в БД ответ на сообщение другого пользователя
     * @param comment - текст ответа.
     * @param author - автор текста.
     * @param note - текущая нота
     * @param commentId - ID комента на который отвечаем.
     */
    public void addReply(String comment, User author, Note note, Comment commentId) {

        Comment newComment = new Comment(author,
                comment,
                note,
                0,
                0,
                note.getProject());

        newComment.setReply(commentId);
        commentRepo.save(newComment);
    }

    /**
     * Метод устанавливает делает перебор списка comment и устанавливает
     * в comment.setDifference() разницу дат между созданием сообщения и текущим моментом
     * @param comment - список коментариев
     * @return - возвращает отформатированный список comment
     */
    public List<Comment> setDifference(List<Comment> comment) {
        Iterator<Comment> commentIterator = comment.iterator();
        while (commentIterator.hasNext()) {
            Comment itteration = commentIterator.next();
            itteration.setDifference(differenceDate(itteration.getDate()));
        }
        return comment;
    }

    /**
     * Метод принимает на вход дату создания записи, <code>LocalDateTime createDay</code>
     * создаем переменную с текущей датой <code>LocalDateTime today = LocalDateTime.now();</code>
     * методом <code>ChronoUnit.....between</code> находим разницу между датами
     * в ГОДАХ, МЕСЯЦАХ, ДНЯХ, ЧАСАХ, МИНУТАХ если <code>long days > 0</code>
     * то строка формируется методом createString() иначе формируем строку HOURS, MINUTES AGO.
     * @param createDay - дата с которой идет сравнение.
     * @return - возвращаем строку String.
     */
    private String differenceDate(LocalDateTime createDay) {
        String message = "";
        LocalDateTime today = LocalDateTime.now();

        long years = ChronoUnit.YEARS.between(createDay,today);
        long month = ChronoUnit.MONTHS.between(createDay,today);
        long days = ChronoUnit.DAYS.between(createDay,today);
        long hours = ChronoUnit.HOURS.between(createDay,today);
        long minutes = ChronoUnit.MINUTES.between(createDay,today);

        if (days>0) {
            message = createString(years,month,days);
        } else {
            if (hours>0) {
                message = minutes/60+" hours, "+minutes%60+" minutes ago.";
            } else {
                message = minutes+" minutes ago.";
            }
        }
        return message;
    }

    /**
     * Метод собирает строку YY_Year, MM_Month, DD_Days ago.
     * @param years - принимает на вход разницу в годах между createDay
     *              и today из метода differenceDate().
     * @param month - принимает на вход разницу в месяцах между createDay
     *              и today из метода differenceDate().
     * @param days - принимает на вход разницу в днях между createDay
     *              и today из метода differenceDate().
     * @return - Возвращает форматированную строку "YY_Year, MM_Month, DD_Days ago."
     */
    private String createString(long years, long month, long days) {
        String message = "";
        if (years>1) {
            message = years+" years ago.";
        } else {
            if (years > 0) {
                if (month%12!=0)
                message = month/12 + " year, "+month%12+" month ago.";
                else {
                    message = month/12 + " year ago.";
                }
            } else {
                if (month > 0) {
                    message = month + " month ago.";
                } else {
                    message = message + days + " days ago.";
                }
            }
        }
            return message;
        }
    }
