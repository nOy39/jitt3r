package org.alpo.example.jitt3r.service;

import org.alpo.example.jitt3r.entity.*;
import org.alpo.example.jitt3r.repos.CommentRepo;
import org.alpo.example.jitt3r.repos.HistoryRepo;
import org.alpo.example.jitt3r.repos.TagRepo;
import org.alpo.example.jitt3r.repos.UploadFileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {
    /**
     * Метод удаляет ноту
     * @param note - нота
     */
    @Autowired
    private HistoryRepo historyRepo;

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private TagRepo tagRepo;

    @Autowired
    private UploadFileRepo uploadFileRepo;

    @Autowired
    private HistoryService historyService;

    /**
     * Метод удаляет все связывающие с Note записи из таблиц
     * TODO сделать при удалении UploadFile, удаление файлов с винта.
     * @param note
     */
    public void deleteNote(Note note) {
        List<History> historyList = historyRepo.findAllByNoteOrderById(note);
        List<Comment> commentList = commentRepo.findAllByNote(note);
        List<Tag> tagList = tagRepo.findAllByNote(note);
        List<UploadFile> uploadFileList = uploadFileRepo.findAllByNote(note);

        historyRepo.deleteAll(historyList);
        commentRepo.deleteAll(commentList);
        uploadFileRepo.deleteAll(uploadFileList);
        tagRepo.deleteAll(tagList);

    }
}
