package org.alpo.example.jitt3r.service;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

@Service
public class ToolService {

    /** getToday()
     * @return - Возвращает текущую дату объектом String,
     *          в формате ("День недели","Число.Месяц.Год","Час:Минуты")
     *          пример ("Чет, 24.05.2018, 09:00")
     */
    public String getToday() {

        SimpleDateFormat sDateFormat = new SimpleDateFormat("EEE, dd.MM.yyyy, HH:mm");

        return sDateFormat.format(new Date());
    }

    /**
     * Метод проверяет корректность выбора даты deadline, чтобы она была больше чем
     * дата сегодня по серверу.
     * @param deadLine
     * @return
     */
    public boolean verifyDate(LocalDate deadLine) {
        boolean isCurrent;
        LocalDate today = LocalDate.now();
        Period period = Period.between(today,deadLine);
        if (period.isNegative()||period.isZero()) {
            System.out.println("wrong date");
            isCurrent = false;
        } else {
            isCurrent = true;
            System.out.println("correct date");
        }
        return isCurrent;
    }
}
