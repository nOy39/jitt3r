package org.alpo.example.jitt3r.controller;

import org.alpo.example.jitt3r.entity.*;
import org.alpo.example.jitt3r.repos.DeskRepo;
import org.alpo.example.jitt3r.repos.HistoryRepo;
import org.alpo.example.jitt3r.repos.NoteRepo;
import org.alpo.example.jitt3r.service.DeskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

//TODO приводим методы в порядок для отображения сообщений и избавляемся от редиректов.
//TODO Задокументировать нужные методы, лишнее удалить
//TODO Сделать удаление дески
//TODO по возможности избавиться от редиректов
@Controller
@RequestMapping(value = "desk")
public class DeskController {

    @Autowired
    DeskRepo deskRepo;

    @Autowired
    NoteRepo noteRepo;

    @Autowired
    DeskService deskService;

    @Autowired
    HistoryRepo historyRepo;


    /**
     * Отображаем страницы досок.
     * @param project
     * @param model
     * @return
     */
    @GetMapping("{project}")
    public String deskTemplateView(@PathVariable Project project,
                                   Model model) {

        List<Desk> desks = deskRepo.findAllByProject(project);
        List<Note> notes = noteRepo.findAllByProject(project);

        model.addAttribute("isDeskForm",true);
        model.addAttribute("notes", notes);
        model.addAttribute("desks",desks);
        model.addAttribute("project",project);

        return "desk";
    }

    @RequestMapping("add")
    public String addDesk(@AuthenticationPrincipal User user,
                          @Valid Desk desk,
                          @RequestParam String deskName,
                          @RequestParam String style,
                          @RequestParam Project project,
                          BindingResult bindingResult,
                          Model model) {

        boolean isDesknameEmpty = StringUtils.isEmpty(deskName);

        if (bindingResult.hasErrors()) {
            Map<String, String> errorMap = ControllerUtil.getErrors(bindingResult);
            model.mergeAttributes(errorMap);
            model.addAttribute("message", desk);
            return "desk";
        }
        return "redirect:/desk/"+project.getId();

    }


    @GetMapping("details/{desk}")
    public String deskEdit(@PathVariable Desk desk,
                           Model model) {
        return "deskedit";
    }

    private Map createModel(Project project, Map<String, Object> model) {

        List<Desk> desks = deskRepo.findAllByProject(project);
        List<Note> notes = noteRepo.findAllByProject(project);

        model.put("isDeskForm",true);
        model.put("notes", notes);
        model.put("desks",desks);
        model.put("project",project);

        return model;
    }
}
