package org.alpo.example.jitt3r.controller;

import org.alpo.example.jitt3r.entity.*;
import org.alpo.example.jitt3r.repos.*;
import org.alpo.example.jitt3r.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;


//TODO: Сделать работу контроллеров без редиректа. (5)
//TODO: Сделать отображение сообщений о выполненных операциях. (6)
//TODO: Навесьти порядок в контроллере, после привидения в должный фунционал Desk
//TODO: Задокументировать методы.


@Controller
@RequestMapping("notes")
public class NoteController {

    @Autowired
    private DeskService deskService;

    @Autowired
    private ToolService toolService;

    @Autowired
    private FileService fileService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private UploadFileRepo uploadFileRepo;

    @Autowired
    private NoteRepo noteRepo;

    @Autowired
    private TagRepo tagRepo;

    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private HistoryRepo historyRepo;

    @Autowired
    private NoteService noteService;

    @Value("${upload.path}")
    private String uploadPath;

    /**
     * noteView() - открывает Note(Нота)
     * @param note - нота со страницы Desk
     * @return - возвращает note.ftl
     */
    @GetMapping(value = "{note}")
    public String notesView(
            @PathVariable Note note,
            Map<String, Object> model) {

        List<Comment> comment = commentService.setDifference(commentRepo.findAllByNoteOrderByDate(note));
        List<Comment> replies = commentService.setDifference(commentRepo.findAllByNoteAndReplyIsNotNull(note));

        commentService.setDifference(comment);

        model.put("countFile",uploadFileRepo.countAllByNote(note));
        model.put("count",commentRepo.countAllByNote(note));
        model.put("histories",historyRepo.findAllByNoteOrderByCreatedDesc(note));
        model.put("images",uploadFileRepo.findAllByNote(note));
        model.put("note", note);
        model.put("tags",tagRepo.findAllByNote(note));
        model.put("comment",comment);
        model.put("replies",replies);
        model.put("today",LocalDate.now());

        return "note";
    }

    /** @PostMapping
     * notesAdd() - Метод создает в таблице новый Note(Нота)
     * @param user - принимает на вход аутефинцированного юзера
     * @param  - Request-запрос принимает на вход Project(Проект),
     *               чтобы к ноте припилить в таблицу project_id
     *               для того, чтобы можно было потом перемещать ноту
     *                внутри проекта между Desk`ами
     * @param desk - Request-запрос принимает на вход Desk(Доска),
     *               чтобы к ноте припилить начальный desk_id,
     *               сортировка в проекте нот по доскам происходит
     *               по колонке из таблицы desk_id.
     * @param noteName - Request-запрос принимает на вход название ноты из
     *                 формы шаблона.
     * @param model - Модель.
     * @return - Возвращает страницу проекта. URL проекта формируется методом getUrl();
     */

    @PostMapping(value = "add")
    public String notesAdd(
            @AuthenticationPrincipal User user,
            @RequestParam Desk desk,
            @RequestParam String noteName,
            Model model) {

        History history;
        if (!noteName.equals("") || !StringUtils.isEmpty(noteName)) {
            Note note = new Note(noteName, LocalDate.now(), user, desk, desk.getProject());
            note.setCreatedDate(LocalDate.now());
            noteRepo.save(note);
            historyService.saveCreatedNote(desk, note, user);
        }
        return deskService.getUrl(desk.getProject().getId());
    }

    /**
     * Добавляем описание к ноте
     * @param user авторизированный пользователь
     * @param note - нота в которой находимся
     * @param description - описание из формы
     * @param model - модель
     * @return
     */
    @PostMapping(value = "description")
    public String notesDescription(
            @AuthenticationPrincipal User user,
            @RequestParam Note note,
            @RequestParam String description,
            Map<String, Object> model) {
        if (!description.equals("")) {
            note.setDescription(description);
            noteRepo.save(note);
            historyService.addDescriptionNote(note,user);
            }
        return "redirect:/notes/"+note.getId();
    }

    /**
     * Добавляем файлы в ноту
     * @param user
     * @param note
     * @param model
     * @param file
     * @return
     */
    @PostMapping(value = "upload")
    public String notesAddBody(
            @AuthenticationPrincipal User user,
            @RequestParam Note note,
            Map<String, Object> model,
            @RequestParam("file") MultipartFile file) {

        UploadFile uploadFile;

        if (fileService.fileExists(file)) {
            uploadFile = fileService.upload(file,"notes");
            uploadFile.setOriginalName(file.getOriginalFilename());
            uploadFile.setNote(note);
            uploadFile.setProject(note.getProject());
            uploadFileRepo.save(uploadFile);

            historyService.uploadFile(uploadFile.getOriginalName(),note,user);
        }

        return "redirect:/notes/"+note.getId();

    }

    /**
     *  Коментарии
     * @param user
     * @param comment
     * @param note
     * @param commentId
     * @param model
     * @return
     */
    @PostMapping(value = "comment")
    public String addMessage(
            @AuthenticationPrincipal User user,
            @RequestParam String comment,
            @RequestParam Note note,
            @RequestParam Comment commentId,
            Model model) {

        if (commentId == null && !comment.equals("")){
            if(commentService.addNewComment(comment, user, note)) {
                historyService.newCommentSave(user,note);
            }
        } else if (!comment.equals("")){
            commentService.addReply(comment, user, note, commentId);
            historyService.replyToComment(note,user,commentId);
        }

        return "redirect:/notes/"+note.getId();
    }

    /**
     *
     * @param user
     * @param note
     * @param date
     * @param model
     * @return
     */
    @PostMapping(value = "dateOperate")
    public String dateOperate(
            @AuthenticationPrincipal User user,
            @RequestParam Note note,
            @RequestParam String date,
            Model model) {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate deadline = LocalDate.parse(date,format);
        note.setDeadLine(deadline);
        return "redirect:/notes/"+note.getId();
    }

    /**
     *
     * @param user
     * @param tag
     * @param note
     * @param model
     * @return
     */
    @PostMapping(value = "addtag")
    public String addTag(
            @AuthenticationPrincipal User user,
            @RequestParam String tag,
            @RequestParam Note note,
            Map<String, Object> model) {
        String message ="";
        if (!tag.equals("")) {
            Tag noteTag = new Tag();
            noteTag.setAuthor(user);
            noteTag.setName(tag);
            noteTag.setNote(note);
            noteTag.setProject(note.getProject());
            tagRepo.save(noteTag);
            message = "New tag added.";
            historyService.addedTags(user,note,tag);
        } else {
            message = "Tag not be empty.";
        }

        model.put("countFile",uploadFileRepo.countAllByNote(note));
        model.put("count",commentRepo.countAllByNote(note));
        model.put("histories",historyRepo.findAllByNoteOrderByCreatedDesc(note));
        model.put("images",uploadFileRepo.findAllByNote(note));
        model.put("note", note);
        model.put("tags",tagRepo.findAllByNote(note));
        model.put("comment",commentRepo.findAllByNoteOrderByDate(note));
        model.put("replies",commentRepo.findAllByNoteAndReplyIsNotNull(note));

        return "redirect:/notes/"+note.getId();
    }

    @PostMapping(value = "{tag}")
    public String deleteTag(
            @AuthenticationPrincipal User user,
            @PathVariable Tag tag,
            Map<String, Object> model) {
        String url = "redirect:/notes/"+tag.getNote().getId();
        historyService.deleteTag(user, tag);
        tagRepo.delete(tag);
        return url;
    }

    /**
     * Метод устанавливает дату DeadLine, проверяет методом <code>verifyDate(deadline)</code>
     * валидность даты и если она валидна сохраняет ее в базу.
     * @param user - авторизироанный пользователь
     * @param note - текущая нотма
     * @param date - дата приходящая с формы
     * @param model - модель
     * @return - возвращает редирект на страницу
     */
    @PostMapping(value = "setDeadline")
    public String setDeadline(
            @AuthenticationPrincipal User user,
            @RequestParam Note note,
            String date,
            Model model) {

        String url = "redirect:/notes/"+note.getId();

        LocalDate deadline = LocalDate.parse(date);
        if (toolService.verifyDate(deadline)) {
            note.setDeadLine(deadline);
            historyService.setDeadline(user, note,date);
            noteRepo.save(note);
        }
        return url;
    }

    /**
     * Устанавливаем ноте статус выполненно.
     * @param user
     * @param note
     * @param confirm
     * @param model
     * @return
     */
    @PostMapping(value = "isDone")
    public String setDone(@AuthenticationPrincipal User user,
                                    @RequestParam Note note,
                                    @RequestParam String confirm,
                                    Model model) {
        if (confirm.toLowerCase().equals("yes")) {
            note.setDone(true);
            historyService.noteIsDone(user,note);
            Tag tag = new Tag();
            tag.setAuthor(user);
            tag.setName("Completed");
            tag.setNote(note);
            tag.setProject(note.getProject());
            tagRepo.save(tag);
            noteRepo.save(note);
        }
        String url = "redirect:/notes/"+note.getId();

        return url;
    }

    @PostMapping(value = "delete")
    public String deleteNotes(@AuthenticationPrincipal User user,
                          @RequestParam Note note,
                          @RequestParam String confirm,
                          Model model) {
        String url = "redirect:/desk/"+note.getProject().getId()+"/list/";
        if (confirm.toLowerCase().equals("yes")) {
            historyService.deleteNote(user, note);
            noteService.deleteNote(note);
            noteRepo.delete(note);
        }

        return url;
    }
}
